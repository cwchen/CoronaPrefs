-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require("widget")

-- Set background to white.
display.setDefault("background", 255 / 255, 255 / 255, 255 / 255)

-- Set valid colors.
local color = {}
color.red = {255, 0, 0}
color.orange = {255, 165, 0}
color.yellow = {255, 255, 0}
color.green = {0, 255, 0}
color.blue = {0, 0, 255}
color.purple = {128, 0, 128}

-- Set valid sizes for different shapes.
local sizeCircle = {}
sizeCircle.small = 30
sizeCircle.medium = 40
sizeCircle.large = 50

local sizeRect = {}
sizeRect.small = {60, 36}
sizeRect.medium = {80, 48}
sizeRect.large = {100, 60}

local sizeTriangle = {}
sizeTriangle.small = {-20, -25, -20, 25, 30, 0}
sizeTriangle.medium = {-30, -35, -30, 35, 40, 0}
sizeTriangle.large = {-40, -45, -40, 45, 50, 0}

local size = {}
size.circle = sizeCircle
size.rectangle = sizeRect
size.triangle = sizeTriangle

-- Set location for our item.
local itemLocX = 0.5
local itemLocY = 0.2

-- Set the list of valid options.
local sizes = { "small", "medium", "large" }
local colors = { "red", "orange", "yellow", "green", "blue", "purple" }
local shapes = { "circle", "rectangle", "triangle" }

local function getShape(options)
  local item

  if options.shape == "circle" then
    item = display.newCircle(options.x, options.y, size[options.shape][options.size])
  elseif options.shape == "rectangle" then
    item = display.newRoundedRect(options.x, options.y,
      size[options.shape][options.size][1], size[options.shape][options.size][2], 10)
  elseif options.shape == "triangle" then
    item = display.newPolygon(options.x, options.y,
      {size[options.shape][options.size][1], size[options.shape][options.size][2],
        size[options.shape][options.size][3], size[options.shape][options.size][4],
        size[options.shape][options.size][5], size[options.shape][options.size][6]})
  end

  item:setFillColor(color[options.color][1] / 255, color[options.color][2] / 255, color[options.color][3] / 255)

  return item
end

local function getIndex(list, item)
  for i = 1, #list do
    if item == list[i] then
      return i
    end
  end
end

-- Get system prefs.
local mySize = system.getPreference("app", "mySize")
if mySize == nil then
  mySize = "medium"
end

local myColor = system.getPreference("app", "myColor")
if myColor == nil then
  myColor = "orange"
end

local myShape = system.getPreference("app", "myShape")

if myShape == nil then
  myShape = "circle"
end

-- Init `item`.
local item = getShape({
  x = display.contentWidth * itemLocX,
  y = display.contentHeight * itemLocY,
  size = mySize,
  color = myColor,
  shape = myShape,
})

-- Set initial columnData by system prefs.
local columnData = {
  {
    align = "left",
    width = 100,
    startIndex = getIndex(sizes, mySize),
    labels = sizes,
  },
  {
    align = "left",
    width = 110,
    startIndex = getIndex(colors, myColor),
    labels = colors,
  },
  {
    align = "left",
    startIndex = getIndex(shapes, myShape),
    labels = shapes,
  }
}

-- Create a PickerWheel widget.
local pickerWheel = widget.newPickerWheel(
  {
    x = display.contentCenterX - 10,
    y = display.contentCenterY + 70,
    columns = columnData,
    fontSize = 18,
    columnColor = {180 / 255, 255 / 255, 255 / 255},
  }
)

-- The event listener for `pickerWheel`.
local onValueSelected = function ()
  local values = pickerWheel:getValues()

  local _size = values[1].value
  local _color = values[2].value
  local _shape = values[3].value

  item:removeSelf()

  item = getShape({
    x = display.contentWidth * itemLocX,
    y = display.contentHeight * itemLocY,
    size = _size,
    color = _color,
    shape = _shape,
  })

  system.setPreferences("app", {
    mySize = _size,
    myColor = _color,
    myShape = _shape,
  })
end

-- Update `item` by current selection.
timer.performWithDelay(100, onValueSelected, -1)
